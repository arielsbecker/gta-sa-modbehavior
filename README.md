## Master

Esta rama contiene el comportamiento normal o por defecto del juego. Sólo he ordenado numéricamente algunos de los parámetros en los archivos .ped de Decision/Allowed para hacer más fácil su comprensión.

### Cómo instalar esto

Simple. Entra a la carpeta donde tienes instalado el GTA San Andreas. Elimina la carpeta `data`. O, si tienes miedo de liarla, limítate a renombrarla (por ejemplo `data_old`).

Luego, ingresa [aquí](https://bitbucket.org/arielsbecker/gta-sa-modbehavior/src) y elige la modificación que más te guste desde el desplegable que dice `master`. `violentos`, por ejemplo, contiene una variación en la cual las pandillas sólo te acosarán verbalmente (a menos que los ataques), mientras el resto de los _peds_ estarán realmente encabronados, y bastará cualquier provocación para hacerlos estallar.

Arriba verás un botón que dice `clone`. Haz clic en el botón que está al lado, el de los puntos suspensivos, y elige `Download repository`. Descomprime el ZIP en la carpeta de instalación del GTA San Andreas, de modo tal que se regenere la carpeta `data`.

Ahora sólo te queda abrir el juego y disfrutar de los cambios.

### Versiones

A medida que yo vaya encontrando variantes interesantes, las subiré aquí a las ramas (branches) correspondientes. También veré de crear un tutorial para que vosotros podáis cambiar la dinámica del juego a gusto.